# sprawl

## About
`sprawl` sets and/or gets environment variables.

## Documentation
#### Installation
Use `sprawl` as you would any CommonJS Node module.

###### Example
```js
const sprawl = require('sprawl');
```

or via destructuring

```js
const { current, isProduction, seize } = require('sprawl');
```

#### Properties
---
### `.current`
Returns the current environment (via `process.env.NODE_ENV`)

###### Example
```js
sprawl.current // => 'test'
```

### `.environments`
Returns an array of valid environments

###### Current valid environments
* `development`
* `test`
* `staging`
* `production`

###### Example
```js
sprawl.environments // => [ 'development', 'production', 'staging', 'test' ]
```

### `.isProduction`
Returns a `boolean` if the current environment is `production`

###### Example
```js
sprawl.isProduction // => true
```

#### Methods
---
### `#seize(name, value)`
Sets an environment variable.

###### Arguments
* `name` &mdash; `<String>`
* `value` &mdash; `<Number>`, `<Object>`, `<String>`

###### Examples
```js
sprawl.seize('JENNYS_PHONENUM', 8675309) // => '8675309'
sprawl.seize('JENNYS_RELEASE_DATE', { development: '1981' }) // => '1981'
sprawl.seize('JENNYS_RECORD_LABEL', 'Columbia Records') // => 'Columbia Records'
```

###### Notes
* All `values` are converted to `<String>`
* If using an `<Object>` as the `value`, only valid `environments` are allowed as keys.

### `#seize(name)`
Returns an environment variable.

###### Arguments
* `name` &mdash; `<String>`

###### Example
```js
sprawl.seize('JENNYS_PHONENUM') // => '8675309'
```
