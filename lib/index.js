const { isObject } = require('lodash');

const current = process.env.NODE_ENV;

const environments = [
    'development',
    'production',
    'staging',
    'test'
];

const isProduction = (current === 'production');

const seize = (name, value) => {

    if (name === undefined || name === null || name === '') {
        throw new Error('Environment variable name required');
    }

    let currentEnvironmentValue = process.env[name];

    if (currentEnvironmentValue === undefined) {
        if (value === undefined || value === null) {
            throw new Error('Environment variable value required');
        }

        if (isObject(value)) {
            const currentValue = value[current];

            if (currentValue === undefined) {
                throw new Error(`${value} contains an unsupported environment`);
            }

            currentEnvironmentValue = currentValue.toString();
        } else {
            currentEnvironmentValue = value.toString();
        }

        process.env[name] = currentEnvironmentValue;
    }

    return currentEnvironmentValue;
};

module.exports = {
    current,
    environments,
    isProduction,
    seize
};
