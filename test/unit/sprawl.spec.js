const { expect } = require('chai');

const {
  current,
  environments,
  isProduction,
  seize
} = require('../../lib');

describe('Sprawl', () => {

  describe('.current', () => {
    it('should return the current environment', () => expect(current).to.equal('test'));
  });

  describe('.environments', () => {
    it('should return a list of supported environments', () => expect(environments).to.include('test'));
  });

  describe('.isProduction', () => {
    it('should return `false` if the current environment is not `production`', () => expect(isProduction).to.equal(false));
  });

  describe('#seize()', () => {
    it('should fail due to missing variable name', () => {
      try {
        seize(undefined, 8675309);
      } catch (err) {
        expect(err.message).to.equal('Environment variable name required');
      }
    });

    it('should fail due to missing variable value', () => {
      try {
        seize('TEST_JENNY_PHONENUM');
      } catch (err) {
        expect(err.message).to.equal('Environment variable value required');
      }
    });

    it('should throw an error if the current environment is unsupported', () => {
      const demo = () => seize('TEST_JENNY_TITLES', { foo: 'bar' });

      expect(demo).to.throw();
    });

    it('should set a environment variable via an Number', () => {
      const demo = seize('TEST_JENNY_PHONENUM', 8675309);

      expect(demo).to.equal('8675309');
    });

    it('should set a environment variable via a Object', () => {
      const demo = seize('TEST_JENNY_TITLE', {
        development: '867-5309',
        test: 'Jenny'
      });

      expect(demo).to.equal('Jenny');
    });

    it('should set a environment variable via a String', () => {
      const demo = seize('TEST_JENNY_CALLER', 'Tommy Tutone');

      expect(demo).to.equal('Tommy Tutone');
    });

    it('should return an environment variable', () => {
      const demo = seize('TEST_JENNY_CALLER');

      expect(demo).to.equal('Tommy Tutone');
    });
  });
});
